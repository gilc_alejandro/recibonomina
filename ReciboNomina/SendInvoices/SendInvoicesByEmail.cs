﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;


namespace ReciboNomina.SendInvoicesByEmail
{
    class SendInvoicesByEmail
    {
        static void Main(string[] args)
        {
            try
            {
                string url = string.Format("{0}{1}", System.Configuration.ConfigurationSettings.AppSettings["BaseUrlDevices"]
                                              , System.Configuration.ConfigurationSettings.AppSettings["SendInvoice"]);

                Console.WriteLine("Enviando los recibos de nómina via email....");
                string ret = CallRestMethod(url);
                if (!string.IsNullOrEmpty(ret))
                {
                    Console.WriteLine("Operación realizada");
                    Console.WriteLine("Presione cualquier tecla para salir");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0}{1}", "Ocurrio un error: ", ex.Message));
                Console.WriteLine("Presione cualquier tecla para salir");
                Console.ReadKey();
            }
        }

        public static string CallRestMethod(string url)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(url);
            webrequest.Method = "GET";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();
            return result;
        }
    }
}
