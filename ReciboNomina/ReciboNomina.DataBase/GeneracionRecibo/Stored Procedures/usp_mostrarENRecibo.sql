﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2016-12-18
-- Description:	Mostrar Detalle Recibo
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_mostrarENRecibo]
	@compania varchar (512),
	@trabajador varchar(512),
	@tipo varchar(2),
	@generatedInvoiceId uniqueidentifier = null
AS
BEGIN
	DECLARE @saveGeneratedInvoice [GeneracionRecibo].EmployeeInvoice

	INSERT INTO @saveGeneratedInvoice
	SELECT 
			H.Nombre
          , H.Excedente
		  , H.Compania
		  , H.Trabajador
		  , h.Cedula
		  , H.Rif
		  , H.Salario
		  , H.nrosso
		  , H.fecha_pago
          , H.periodo
		  , H.PeriodoIni
		  , H.PeriodoFin
		  , H.Anio
		  , 0
		  , H.Cuenta
	FROM Fpv_RecNomHdr H
	WHERE ltrim(rtrim(compania)) = CASE WHEN @tipo='T' THEN ltrim(rtrim(@compania)) ELSE compania END  
	  AND ltrim(rtrim(trabajador)) = ltrim(rtrim(@trabajador))
	ORDER BY trabajador;

	DECLARE @userId uniqueidentifier = null
	      , @errorMessage varchar(max)
		  , @lastGeneratedInvoicesId uniqueidentifier
	
	SELECT @userId = userId FROM [GeneracionRecibo].tbl_Users U WHERE U.userName = 'admin';

	EXEC [GeneracionRecibo].[usp_SaveEmployeeInvoice]  @saveGeneratedInvoice, @userId, @generatedInvoiceId, @trabajador, @errorMessage OUT, @lastGeneratedInvoicesId OUTPUT;

	IF @errorMessage <> 'Success' BEGIN
		RAISERROR (60002, 16, 1)
		--  EXEC sp_addmessage 60002, 16,   
		--	N'There was an error attempting to save the invoices in the Database';  
		RETURN;
	END
	SELECT	*, @lastGeneratedInvoicesId AS LastGeneratedInvoicesId
	FROM Fpv_RecNomHdr H
	WHERE ltrim(rtrim(compania)) = CASE WHEN @tipo='T' THEN ltrim(rtrim(@compania)) ELSE compania END  
	  AND ltrim(rtrim(trabajador)) = ltrim(rtrim(@trabajador))
	ORDER BY trabajador;
END