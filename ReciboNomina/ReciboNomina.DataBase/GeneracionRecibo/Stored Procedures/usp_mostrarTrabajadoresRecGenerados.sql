﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2017-02-13
-- Description:	Get Employees By Company
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_mostrarTrabajadoresRecGenerados] 
	@compania varchar(512)
AS
BEGIN
	SELECT DISTINCT 
					  HDR.trabajador
					, HDR.Excedente as Correo
					, HDR.compania 
					, HDR.Nombre
					, HDR.periodo
					, HDR.PeriodoIni
					, HDR.PeriodoFin
					, HDR.Anio
	FROM Fpv_RecNomHdr HDR
	WHERE compania = @compania;
END