﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2017-03-05
-- Description:	Save Generated Invoices
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_SaveEmployeeInvoice]
	@employeeInvoice [GeneracionRecibo].EmployeeInvoice READONLY,
	@userId Uniqueidentifier,
	@generatedInvoicesId Uniqueidentifier = NULL,
	@employeeCode varchar(512),
	@errorMessage varchar(max) = 'Success' OUTPUT,
	@lastGeneratedInvoicesId uniqueidentifier OUTPUT
 AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @toSave TABLE ( 
							[id] uniqueidentifier not null default newsequentialid()
						   ,[employeeFullName] varchar(255) not null
						   ,[employeeEmail] varchar(255) not null
						   ,[company] varchar(10) not null
						   ,[employeeCode] varchar(255) not null
						   ,[employeeId] varchar(255) not null
						   ,[employeeRif] varchar(255) not null
						   ,[employeeIncomes] float not null
						   ,[employeeSSO] varchar(255) not null
						   ,[employeePaymentDate] datetime
						   ,[period] int not null
						   ,[startPeriod] datetime NOT NULL
						   ,[endPeriod] datetime NOT NULL
						   ,[year] int not null
						   ,[sentEmail] bit not null
						   ,[employeeAccountNumber] VARCHAR(255)
						  )
	DECLARE @generatedInvoicesFK uniqueidentifier = null;

	IF @generatedInvoicesId IS NULL
	BEGIN
	
	--BEGIN TRY;
	--BEGIN TRAN SaveGeneratedInvoices;

		INSERT INTO @toSave
							  ( 
									    [employeeFullName] 
									   ,[employeeEmail] 
									   ,[company] 
									   ,[employeeCode] 
									   ,[employeeId] 
									   ,[employeeRif]
									   ,[employeeIncomes] 
									   ,[employeeSSO] 
									   ,[employeePaymentDate]
									   ,[period] 
									   ,[startPeriod] 
									   ,[endPeriod] 
									   ,[year] 
									   ,[sentEmail] 
									   ,[employeeAccountNumber] 
							  )
		OUTPUT    [inserted].id 
				, [inserted].[company]
				, [inserted].[period]
				, [inserted].[startPeriod]
				, [inserted].[endPeriod]
				, [inserted].[year]
				, [inserted].[sentEmail]
		INTO [GeneracionRecibo].[tbl_GeneratedInvoices]
		SELECT 
			 E.[employeeFullName] 
			,E.[employeeEmail] 
			,E.[company] 
			,E.[employeeCode] 
			,E.[employeeId] 
			,E.[employeeRif]
			,E.[employeeIncomes] 
			,E.[employeeSSO] 
			,E.[employeePaymentDate]
			,E.[period] 
			,E.[startPeriod] 
			,E.[endPeriod] 
			,E.[year] 
			,E.[sentEmail] 
			,E.[employeeAccountNumber] 
		 FROM @employeeInvoice AS E

		 SELECT @generatedInvoicesFK = S.id FROM @toSave S;
		
	END
	ELSE
	BEGIN
		INSERT INTO @toSave
							  ( 
								 [employeeFullName] 
								,[employeeEmail] 
								,[company] 
								,[employeeCode] 
								,[employeeId] 
								,[employeeRif]
								,[employeeIncomes] 
								,[employeeSSO] 
								,[employeePaymentDate]
								,[period] 
								,[startPeriod] 
								,[endPeriod] 
								,[year] 
								,[sentEmail] 
								,[employeeAccountNumber] 
							  )
		SELECT 
			 E.[employeeFullName] 
			,E.[employeeEmail] 
			,E.[company] 
			,E.[employeeCode] 
			,E.[employeeId] 
			,E.[employeeRif]
			,E.[employeeIncomes] 
			,E.[employeeSSO] 
			,E.[employeePaymentDate]
			,E.[period] 
			,E.[startPeriod] 
			,E.[endPeriod] 
			,E.[year] 
			,E.[sentEmail] 
			,E.[employeeAccountNumber]
		 FROM @employeeInvoice AS E
		 SET @generatedInvoicesFK = @generatedInvoicesId;
	END
	
	SET @lastGeneratedInvoicesId = @generatedInvoicesFK;
	
	INSERT INTO [GeneracionRecibo].[tbl_GeneratedInvoicesDetails]
	(	
		  [generatedInvoicesId]
		, [employeeFullName]
        , [employeeCode]
		, [employeeEmail]
        , [employeeId]
		, [employeeRif]
		, [employeeIncomes]
		, [employeeSSO]
		, [employeePaymentDate]
		, [employeeAccountNumber]
		, [sentEmail]
        --, [employeeInvoiceDescription]
		--, [employeeInvoiceCodeType]
		--, [employeeInvoiceAmount]
		--, [employeeInvoiceTime]
		--, [employeeInvoiceAvailable]
		, [sysCreationBy]
		, [sysModifyBy]
	)
	(
		SELECT 
			@generatedInvoicesFK
		  ,	S.employeeFullName
	      , @employeeCode
		  , S.employeeEmail
		  , S.employeeId
		  , S.employeeRif
		  , S.employeeIncomes
		  , S.employeeSSO
		  , S.employeePaymentDate
		  , S.[employeeAccountNumber]
		  , 0
		  --, DET.Descripcion
		  --, DET.TipoCodigo
		  --, DET.Monto
		  --, DET.Tiempo
		  --, DET.Saldo
		  , @userId
		  , @userId
		FROM   @toSave S
	)
	DECLARE @generatedInvoicesDetailsFK uniqueidentifier = null;

	SELECT @generatedInvoicesDetailsFK = d.id
	FROM   [GeneracionRecibo].tbl_GeneratedInvoicesDetails D 
	WHERE  D.generatedInvoicesId = @generatedInvoicesFK AND LTRIM(RTRIM(D.employeeCode)) = LTRIM(RTRIM(@employeeCode))

	INSERT INTO [GeneracionRecibo].tbl_GeneratedInvoicesAssignments(
				 employeeInvoiceAmount
				,employeeInvoiceAmountAvailable
				,employeeInvoiceCodeType
				,employeeInvoiceDescription
				,employeeInvoiceTime
				,generatedInvoiceDetails
				,sysCreationBy
				,sysModifyBy
	)
	(
		SELECT   DET.Monto
				,ISNULL(DET.Saldo, 0)
				,DET.TipoCodigo
				,DET.Descripcion
				,DET.Tiempo
				,@generatedInvoicesDetailsFK
				,@userId
				,@userId
		FROM  [dbo].Fpv_RecNomDet DET
		WHERE LTRIM(RTRIM(DET.Trabajador)) = LTRIM(RTRIM(@employeeCode))
	)
	
	--COMMIT TRAN SaveGeneratedInvoices;
	--END TRY
	--BEGIN CATCH
	--	SET @errorMessage  = 'Error on the line:' + CAST(ERROR_LINE() as varchar(5)) + '  Error message: ' + ERROR_MESSAGE();
	  --ROLLBACK TRAN SaveGeneratedInvoices;
	--END CATCH
END