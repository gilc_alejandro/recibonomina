﻿CREATE PROCEDURE [GeneracionRecibo].[usp_GetPayrollInvoicesById]
@id uniqueidentifier
AS
BEGIN
	SELECT
	   HEAD.company
	  ,HEAD.period
	  ,HEAD.startPeriod
	  ,HEAD.endPeriod
	  ,HEAD.[year]
	  ,DET.employeeFullName
	  ,DET.employeeCode
	  ,DET.employeeEmail
	  ,DET.employeeId
	  ,DET.employeeRif
	  ,DET.employeeIncomes
	  ,DET.employeeSSO
	  ,DET.employeePaymentDate
	  ,DET.employeeAccountNumber
	  ,ASSIG.[employeeInvoiceDescription]
	  ,ASSIG.[employeeInvoiceCodeType]
	  ,ASSIG.[employeeInvoiceAmount]
	  ,ASSIG.[employeeInvoiceTime]
	  ,ASSIG.[employeeInvoiceAmountAvailable]
	  ,  
	     (SELECT SUM([employeeInvoiceAmount]) FROM GeneracionRecibo.tbl_GeneratedInvoicesAssignments WHERE tbl_GeneratedInvoicesAssignments.employeeInvoiceCodeType = 1 AND ASSIG.generatedInvoiceDetails = generatedInvoiceDetails) -
	     (SELECT SUM([employeeInvoiceAmount]) FROM GeneracionRecibo.tbl_GeneratedInvoicesAssignments WHERE tbl_GeneratedInvoicesAssignments.employeeInvoiceCodeType = 2  AND ASSIG.generatedInvoiceDetails = generatedInvoiceDetails) AS MontoTotal
	FROM
	  GeneracionRecibo.tbl_GeneratedInvoices HEAD
	  JOIN GeneracionRecibo.tbl_GeneratedInvoicesDetails DET
	 ON HEAD.id = DET.generatedInvoicesId
	 JOIN GeneracionRecibo.tbl_GeneratedInvoicesAssignments ASSIG 
	 ON ASSIG.generatedInvoiceDetails = DET.id 
	WHERE
	  HEAD.id = ISNULL(@id, HEAD.id);
END