﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2016-12-18
-- Description:	Get Employees By Company
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].usp_mostrarTrabajadores 
	@trabajador varchar(256),
	@compania varchar(512)
AS
BEGIN
	select * 
	from vw_trabajadores_full 
	where ltrim(rtrim(trabajador)) =  @trabajador and compania = @compania;
END
