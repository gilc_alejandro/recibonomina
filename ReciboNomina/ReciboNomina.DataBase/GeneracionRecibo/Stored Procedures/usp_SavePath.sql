﻿CREATE PROCEDURE [GeneracionRecibo].[usp_SavePath]
	@id uniqueidentifier,
	@invoicePath varchar(1024),
	@employeeFullName VARCHAR(512)
AS
BEGIN
	UPDATE [GeneracionRecibo].[tbl_GeneratedInvoicesDetails]
	SET    [invoicePath] = @invoicePath, sysModifyDate = GETDATE()
	WHERE  [generatedInvoicesId] = @id AND LTRIM(RTRIM(employeeFullName)) = @employeeFullName;
END