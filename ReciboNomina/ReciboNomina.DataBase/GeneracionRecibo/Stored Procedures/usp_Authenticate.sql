﻿
-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2016-12-18
-- Description: AUTHENTICATE USER
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_Authenticate]
	@userName varchar(256),
	@userPassword varchar(256)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT [userId], [userName], [userPassword], [adUserName]
	FROM   [GeneracionRecibo].[tbl_Users] 
	WHERE  [userName] = @userName AND [userPassword] = @userPassword;
END

GO