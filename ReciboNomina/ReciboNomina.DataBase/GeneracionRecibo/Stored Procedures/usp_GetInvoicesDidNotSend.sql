﻿CREATE PROCEDURE [GeneracionRecibo].[usp_GetInvoicesDidNotSend]
AS
BEGIN
	DECLARE @ret uniqueidentifier
	SELECT @ret = G.id FROM [GeneracionRecibo].tbl_GeneratedInvoices G WHERE sentEmail = 0;

	IF @@ROWCOUNT = 0
	   SELECT '';
	ELSE
		SELECT @ret;
END