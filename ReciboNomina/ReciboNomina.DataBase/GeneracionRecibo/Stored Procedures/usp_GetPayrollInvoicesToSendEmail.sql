﻿CREATE PROCEDURE [GeneracionRecibo].[usp_GetPayrollInvoicesToSendEmail]
@id uniqueidentifier
AS
BEGIN
	SELECT

	   DET.employeeFullName
	  ,DET.employeeEmail
	  ,G.year
	  ,G.period
	FROM
	   GeneracionRecibo.tbl_GeneratedInvoicesDetails DET
	   JOIN 
	   GeneracionRecibo.tbl_GeneratedInvoices G
	   ON G.id = DET.generatedInvoicesId
	WHERE
	  DET.generatedInvoicesId = ISNULL(@id, DET.generatedInvoicesId);
END