﻿CREATE PROCEDURE [GeneracionRecibo].[usp_SetInvoiceAsSent]
@id uniqueidentifier
As
Begin
	UPDATE [GeneracionRecibo].tbl_GeneratedInvoices SET sentEmail = 1 WHERE id = @id;

	UPDATE [GeneracionRecibo].tbl_GeneratedInvoicesDetails SET sentEmail = 1, sysModifyDate = GETDATE() WHERE id = @id;
end