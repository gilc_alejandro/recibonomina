﻿-- =============================================
-- Author:		Ivan Pocaterra
-- Create date: 20/01/2015
-- Description:	List Validate User Authorization
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_GetValidateListUserAuthorization]
	@userId uniqueidentifier,
	@ValidateFunctionality BIT = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @userIsSystem BIT
	DECLARE @isDeleted BIT
	
	SET @userIsSystem = 1
	SET @isDeleted = 0
	
	-- Check if user is System or partner.
	IF (EXISTS(SELECT UR.roleId FROM GeneracionRecibo.tbl_Users_Roles AS UR INNER JOIN
                      GeneracionRecibo.tbl_Roles AS R ON UR.roleId = R.roleId
                   WHERE UR.userId = @userId AND R.sysIsSystem = 0))
	BEGIN
		SET @userIsSystem = 0
	END
	
	-- Check Permission Functionality	
	IF (@ValidateFunctionality = 1)
	BEGIN
	   SELECT DISTINCT 0 as aclId, functionalityControllerName, functionalityActionName FROM GeneracionRecibo.tbl_ACLs AS A INNER JOIN
					 GeneracionRecibo.tbl_Functionalities AS F ON A.aclSecurableId = F.functionalityId 
					  AND F.sysIsDeleted = @isDeleted
		WHERE A.aclIsSystem = @userIsSystem AND A.aclPrincipalType = 'R' AND A.aclSecurableType = 'F'
			AND A.aclIsGranted = 1 AND A.sysIsDeleted = @isDeleted 
			AND A.aclPrincipalId IN (SELECT roleId FROM GeneracionRecibo.tbl_Users_Roles 
										WHERE userId = @userId AND sysIsDeleted = @isDeleted)
	END
	

END