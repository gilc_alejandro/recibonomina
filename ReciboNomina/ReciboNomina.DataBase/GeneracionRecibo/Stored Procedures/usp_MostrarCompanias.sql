﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2016-12-18
-- Description:	Get Companies By User
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].usp_MostrarCompanias
	@usuario varchar(256)
AS
BEGIN
	SET NOCOUNT ON;

    Select distinct compania, (select ltrim(rtrim(nombre_cia)) 
	From companias 
	Where compania= aut_companias.compania) as nombre_cia, usuario 
	From   Aut_Companias  
	Where  Usuario = @usuario;
END
