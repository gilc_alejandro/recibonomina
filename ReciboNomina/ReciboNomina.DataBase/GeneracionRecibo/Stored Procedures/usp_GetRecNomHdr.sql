﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2017-03-16
-- Description: Get Recibo Nomina Header
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].usp_GetRecNomHdr
    @compania varchar (512),
	@trabajador varchar(512),
	@tipo varchar(2)
AS
BEGIN
	SELECT	*
	FROM Fpv_RecNomHdr H
	WHERE ltrim(rtrim(compania)) = CASE WHEN @tipo='T' THEN ltrim(rtrim(@compania)) ELSE compania END  
	  AND ltrim(rtrim(trabajador)) = ltrim(rtrim(@trabajador))
	ORDER BY trabajador;
END