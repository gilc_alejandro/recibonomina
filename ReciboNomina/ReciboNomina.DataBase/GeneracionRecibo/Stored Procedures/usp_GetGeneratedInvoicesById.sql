﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2017-03-13
-- Description:	GetGeneratedInvoicesById
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_GetGeneratedInvoicesById]
	@id uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	   CASE WHEN HEAD.company = 1 THEN 'Schneider Electric De Venezuela, S.A.' ELSE 'Schneider Electric System, C.A.' END AS [Compania]
	  ,HEAD.[year] AS [Año]
	  ,HEAD.period AS [Periodo]
	  ,HEAD.startPeriod AS [Fecha Inicio]
	  ,HEAD.endPeriod AS [Fecha Fin]
	  ,DET.employeeFullName AS [Trabajador]
	  ,DET.employeeCode AS [Codigo Trabajador]
	  ,DET.employeeId AS [Cedula]
	  ,DET.employeeIncomes AS [Sueldo Mensual]
	  ,DET.employeePaymentDate AS [Fecha de Pago]
	  ,DET.sentEmail AS [Enviado]
	  ,DET.employeeEmail AS [Email]
	  ,DET.invoicePath
	FROM
	  GeneracionRecibo.tbl_GeneratedInvoices HEAD
	  JOIN GeneracionRecibo.tbl_GeneratedInvoicesDetails DET
	 ON HEAD.id = DET.generatedInvoicesId
	 JOIN GeneracionRecibo.tbl_GeneratedInvoicesAssignments ASSIG 
	 ON ASSIG.generatedInvoiceDetails = DET.id 
	WHERE
	  HEAD.id = ISNULL(@id, HEAD.id)
	  GROUP BY 
		   HEAD.company 
		  ,HEAD.[year] 
		  ,HEAD.period 
		  ,HEAD.startPeriod 
		  ,HEAD.endPeriod 
		  ,DET.employeeFullName 
		  ,DET.employeeCode
		  ,DET.employeeId 
		  ,DET.employeeIncomes
		  ,DET.employeePaymentDate 
		  ,DET.sentEmail 
		  ,DET.employeeEmail
		  ,DET.invoicePath
	RETURN;
END