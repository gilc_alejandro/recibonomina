﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2017-02-12
-- Description:	Mostrar Clase de Nomina
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_clases_nomina]
	
AS
BEGIN
	SELECT clase_nomina, descripcion FROM dbo.clases_nomina
END