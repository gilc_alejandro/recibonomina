﻿CREATE PROCEDURE [GeneracionRecibo].[usp_GetInvoicesDidNotSendDetails]
AS
BEGIN
	SELECT 
		   CASE WHEN HEAD.company = 1 THEN 'Schneider Electric De Venezuela, S.A.' ELSE 'Schneider Electric System, C.A.' END AS [Compania]
		  ,HEAD.[year] AS [Año]
		  ,HEAD.period AS [Periodo]
		  ,HEAD.startPeriod AS [Fecha Inicio]
		  ,HEAD.endPeriod AS [Fecha Fin]
		  ,DET.employeeFullName AS [Trabajador]
		  ,DET.employeeCode AS [Codigo Trabajador]
		  ,DET.employeeId AS [Cedula]
		  ,DET.employeeIncomes AS [Sueldo Mensual]
		  ,DET.employeePaymentDate AS [Fecha de Pago]
		  ,DET.sentEmail AS [Enviado]
		  ,DET.employeeEmail AS [Email]
	FROM [GeneracionRecibo].tbl_GeneratedInvoicesDetails DET JOIN [GeneracionRecibo].tbl_GeneratedInvoices HEAD ON DET.generatedInvoicesId = HEAD.id
	WHERE DET.sentEmail = 0;
END