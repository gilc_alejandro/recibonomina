﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2016-12-18
-- Description:	Mostrar Detalle Recibo
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].[usp_mostrarDetalleRecibo]
	@compania varchar (512),
	@trabajador varchar(512)
AS
BEGIN
	SELECT 
			  Compania
			, Trabajador
			, Codigo
			, TipoCodigo
			, Descripcion
			, Monto
			, Monto_bono
			, Tiempo
			, Saldo
			, Referencia
			, Ref_trans
			, sit_transaccion
			, Id_Calendario
			, Id_Session
			, [No_RECIBO ]
			, Unidad_tiempo
			,(SELECT SUM(Monto) FROM Fpv_RecNomDet WHERE TipoCodigo = 1 AND ltrim(rtrim(trabajador)) = @trabajador ) - ISNULL((SELECT SUM(Monto) FROM Fpv_RecNomDet WHERE TipoCodigo = 2 AND ltrim(rtrim(trabajador)) = @trabajador),0) MontoTotal
	FROM Fpv_RecNomDet 
	WHERE ltrim(rtrim(trabajador)) = @trabajador 
	  AND ltrim(rtrim(compania)) = @compania and TipoCodigo in (1,2) 
    ORDER BY codigo
END
