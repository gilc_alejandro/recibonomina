﻿-- =============================================
-- Author:		Alejandro Gil
-- Create date: 2016-12-18
-- Description:	Get Tipo de Nomina
-- =============================================
CREATE PROCEDURE [GeneracionRecibo].usp_mostrarTipoNomina
	
AS
BEGIN
	Select tipo_nomina, descripcion 
	From tipos_nomina
	Where manejo = 1 
    Order by tipo_nomina
END
