﻿-- =============================================
-- Author:		Johan Cuello
-- Create date: 11/27/2011
-- Description:	Returns the roles from a user
-- =============================================
CREATE PROCEDURE GeneracionRecibo.[usp_GetRolesForUser]
	@userName VARCHAR(512)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @isDeleted bit
	SET @isDeleted=0
	
	SELECT R.[roleId]
		  ,[roleName]
		  ,[roleDescription]
		  ,[applicationName]
		  ,R.[sysCreationDate]
		  ,R.[sysCreatedBy]
		  ,R.[sysModifyDate]
		  ,R.[sysModifiedBy]
		  ,R.[sysIsSystem]
	FROM GeneracionRecibo.[tbl_Roles] R
	    JOIN GeneracionRecibo.[tbl_Users_Roles] UR ON R.roleId=UR.roleId
	    JOIN GeneracionRecibo.[tbl_Users] U	ON U.userId=UR.userId
	WHERE U.userName = @userName
	AND R.sysIsDeleted = @isDeleted
	AND UR.sysIsDeleted = @isDeleted;
	
END