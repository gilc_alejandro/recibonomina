﻿CREATE TYPE [GeneracionRecibo].[EmployeeInvoice] AS TABLE (
    [employeeFullName]      VARCHAR (255) NOT NULL,
    [employeeEmail]         VARCHAR (50)  NOT NULL,
    [company]               VARCHAR (50)  NOT NULL,
    [employeeCode]          VARCHAR (255) NOT NULL,
    [employeeId]            VARCHAR (50)  NULL,
    [employeeRif]           VARCHAR (50)  NULL,
    [employeeIncomes]       FLOAT (53)    NULL,
    [employeeSSO]           VARCHAR (50)  NULL,
    [employeePaymentDate]   DATETIME      NULL,
    [period]                INT           NOT NULL,
    [startPeriod]           DATETIME      NOT NULL,
    [endPeriod]             DATETIME      NOT NULL,
    [year]                  INT           NOT NULL,
    [sentEmail]             BIT           NOT NULL,
    [employeeAccountNumber] VARCHAR (255) NULL);





