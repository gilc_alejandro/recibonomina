﻿CREATE TABLE [GeneracionRecibo].[tbl_Users_Roles] (
    [userId]          UNIQUEIDENTIFIER NOT NULL,
    [roleId]          UNIQUEIDENTIFIER NOT NULL,
    [sysCreationDate] DATETIME         CONSTRAINT [DF_tbl_Users_Roles_sysCreationDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysCreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [sysModifyDate]   DATETIME         CONSTRAINT [DF_tbl_Users_Roles_sysModifyDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [sysIsDeleted]    BIT              CONSTRAINT [DF_tbl_Users_Roles_sysIsDeleted] DEFAULT ((0)) NOT NULL,
    [sysIsSystem]     BIT              CONSTRAINT [DF_tbl_Users_Roles_sysIsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_Users_Roles] PRIMARY KEY CLUSTERED ([userId] ASC, [roleId] ASC),
    CONSTRAINT [CK_tbl_Users_Roles_CanDelete] CHECK (NOT ([sysIsSystem]=(1) AND [sysIsDeleted]=(1))),
    CONSTRAINT [FK_tbl_Roles_roleId_tbl_Users_Roles_roleId] FOREIGN KEY ([roleId]) REFERENCES [GeneracionRecibo].[tbl_Roles] ([roleId])
);



