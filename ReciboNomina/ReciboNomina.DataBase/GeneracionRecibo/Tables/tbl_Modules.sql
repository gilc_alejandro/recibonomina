﻿CREATE TABLE [GeneracionRecibo].[tbl_Modules] (
    [moduleId]          UNIQUEIDENTIFIER    CONSTRAINT [tbl_ModulesNEWID_] DEFAULT (newid()) NOT NULL,
    [moduleName]        [dbo].[name]        NOT NULL,
    [moduleDescription] [dbo].[description] NULL,
    [applicationId]     UNIQUEIDENTIFIER    NOT NULL,
    [sysCreationDate]   DATETIME            CONSTRAINT [DF_tbl_Modules_sysCreationDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysCreatedBy]      UNIQUEIDENTIFIER    NOT NULL,
    [sysModifyDate]     DATETIME            CONSTRAINT [DF_tbl_Modules_sysModifyDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysModifiedBy]     UNIQUEIDENTIFIER    NOT NULL,
    [sysIsDeleted]      BIT                 NOT NULL,
    [sysIsSystem]       BIT                 CONSTRAINT [DF_tbl_Modules_sysIsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_Modules] PRIMARY KEY CLUSTERED ([moduleId] ASC),
    CONSTRAINT [IX_tbl_Modules_modName] UNIQUE NONCLUSTERED ([moduleName] ASC, [applicationId] ASC)
);





