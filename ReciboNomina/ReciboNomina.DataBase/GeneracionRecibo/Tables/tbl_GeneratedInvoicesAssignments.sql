﻿CREATE TABLE [GeneracionRecibo].[tbl_GeneratedInvoicesAssignments] (
    [id]                             UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_GeneratedInvoicesAssignments_id] DEFAULT (newsequentialid()) NOT NULL,
    [generatedInvoiceDetails]        UNIQUEIDENTIFIER NOT NULL,
    [generatedInvoice]               UNIQUEIDENTIFIER NULL,
    [employeeInvoiceDescription]     VARCHAR (255)    NOT NULL,
    [employeeInvoiceCodeType]        INT              NOT NULL,
    [employeeInvoiceAmount]          FLOAT (53)       NOT NULL,
    [employeeInvoiceTime]            FLOAT (53)       NOT NULL,
    [employeeInvoiceAmountAvailable] FLOAT (53)       NOT NULL,
    [sysCreationDate]                DATETIME         CONSTRAINT [DF_tbl_GeneratedInvoicesAssignments_sysCreationDate] DEFAULT (getdate()) NOT NULL,
    [sysCreationBy]                  UNIQUEIDENTIFIER NOT NULL,
    [sysModifyDate]                  DATETIME         CONSTRAINT [DF_tbl_GeneratedInvoicesAssignments_sysModifyDate] DEFAULT (getdate()) NOT NULL,
    [sysModifyBy]                    UNIQUEIDENTIFIER NOT NULL,
    [sysIsDeleted]                   BIT              CONSTRAINT [DF_tbl_GeneratedInvoicesAssignments_sysIsDeleted] DEFAULT ((0)) NOT NULL,
    [sysIsSystem]                    BIT              CONSTRAINT [DF_tbl_GeneratedInvoicesAssignments_sysIsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_GeneratedInvoicesAssignments] PRIMARY KEY CLUSTERED ([id] ASC)
);



