﻿CREATE TABLE [GeneracionRecibo].[tbl_Functionalities] (
    [functionalityId]             UNIQUEIDENTIFIER    CONSTRAINT [NEWID] DEFAULT (newid()) NOT NULL,
    [functionalityName]           [dbo].[name]        NOT NULL,
    [functionalityDescription]    [dbo].[description] NOT NULL,
    [moduleId]                    UNIQUEIDENTIFIER    NOT NULL,
    [functionalityControllerName] [dbo].[name]        NOT NULL,
    [functionalityActionName]     [dbo].[name]        NOT NULL,
    [applicationId]               UNIQUEIDENTIFIER    NOT NULL,
    [sysCreationDate]             DATETIME            CONSTRAINT [DF_tbl_Funcionalities_sysCreationDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysCreatedBy]                UNIQUEIDENTIFIER    NOT NULL,
    [sysModifyDate]               DATETIME            CONSTRAINT [DF_tbl_Funcionalities_sysModifyDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysModifiedBy]               UNIQUEIDENTIFIER    NOT NULL,
    [sysIsDeleted]                BIT                 CONSTRAINT [DF_tbl_Funcionalities_sysIsDeleted] DEFAULT ((0)) NOT NULL,
    [sysIsSystem]                 BIT                 CONSTRAINT [DF_tbl_Functionalities_sysIsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_Funcionalities] PRIMARY KEY CLUSTERED ([functionalityId] ASC),
    CONSTRAINT [FK_tbl_Functionalities_tbl_Modules] FOREIGN KEY ([moduleId]) REFERENCES [GeneracionRecibo].[tbl_Modules] ([moduleId])
);





