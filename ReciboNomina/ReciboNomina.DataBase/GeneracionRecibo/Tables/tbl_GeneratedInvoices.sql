﻿CREATE TABLE [GeneracionRecibo].[tbl_GeneratedInvoices] (
    [id]          UNIQUEIDENTIFIER NOT NULL,
    [company]     VARCHAR (10)     NOT NULL,
    [period]      INT              NOT NULL,
    [startPeriod] DATETIME         NOT NULL,
    [endPeriod]   DATETIME         NOT NULL,
    [year]        INT              NOT NULL,
    [sentEmail]   BIT              CONSTRAINT [DF_tbl_GeneratedInvoices_sentEmail] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_tbl_GeneratedInvoices] PRIMARY KEY CLUSTERED ([id] ASC)
);







