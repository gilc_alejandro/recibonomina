﻿CREATE TABLE [GeneracionRecibo].[tbl_GeneratedInvoicesDetails] (
    [id]                    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_GeneratedInvoicesDetails_id] DEFAULT (newsequentialid()) NOT NULL,
    [generatedInvoicesId]   UNIQUEIDENTIFIER NOT NULL,
    [employeeFullName]      VARCHAR (255)    NOT NULL,
    [employeeCode]          VARCHAR (255)    NULL,
    [employeeEmail]         VARCHAR (50)     NOT NULL,
    [employeeId]            VARCHAR (255)    NULL,
    [employeeRif]           VARCHAR (255)    NULL,
    [employeeIncomes]       FLOAT (53)       NULL,
    [employeeSSO]           VARCHAR (255)    NULL,
    [employeePaymentDate]   DATETIME         NULL,
    [employeeAccountNumber] VARCHAR (255)    NULL,
    [sentEmail]             BIT              NOT NULL,
    [invoicePath]           VARCHAR(1024)    NOT NULL,
	[sysCreationDate]       DATETIME         CONSTRAINT [DF_tbl_GeneratedInvoicesDetails_sysCreationDate] DEFAULT (getdate()) NOT NULL,
    [sysCreationBy]         UNIQUEIDENTIFIER NOT NULL,
    [sysModifyDate]         DATETIME         CONSTRAINT [DF_tbl_GeneratedInvoicesDetails_sysModifyDate] DEFAULT (getdate()) NOT NULL,
    [sysModifyBy]           UNIQUEIDENTIFIER NOT NULL,
    [sysIsDeleted]          BIT              CONSTRAINT [DF_tbl_GeneratedInvoicesDetails_sysIsdeleted] DEFAULT ((0)) NOT NULL,
    [sysIsSystem]           BIT              CONSTRAINT [DF_tbl_GeneratedInvoicesDetails_sysIsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [pk_GeneratedInvoicesDetailsid] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 90)
);





