﻿CREATE TABLE [GeneracionRecibo].[tbl_Users] (
    [userId]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Users_userId] DEFAULT (newid()) NOT NULL,
    [userName]     VARCHAR (255)    NOT NULL,
    [userPassword] VARCHAR (255)    NOT NULL,
    [adUserName]   VARCHAR (255)    NULL,
    CONSTRAINT [pk_userId] PRIMARY KEY CLUSTERED ([userId] ASC) WITH (FILLFACTOR = 90)
);



GO


GO


