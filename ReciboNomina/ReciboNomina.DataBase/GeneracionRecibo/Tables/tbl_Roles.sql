﻿CREATE TABLE [GeneracionRecibo].[tbl_Roles] (
    [roleId]          UNIQUEIDENTIFIER    CONSTRAINT [DF_NEW_ID] DEFAULT (newid()) NOT NULL,
    [roleName]        [dbo].[name]        NOT NULL,
    [roleDescription] [dbo].[description] NULL,
    [sysCreationDate] DATETIME            CONSTRAINT [DF_tbl_Roles_sysCreationDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysCreatedBy]    UNIQUEIDENTIFIER    NOT NULL,
    [sysModifyDate]   DATETIME            CONSTRAINT [DF_tbl_Roles_sysModifyDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysModifiedBy]   UNIQUEIDENTIFIER    NOT NULL,
    [sysIsSystem]     BIT                 CONSTRAINT [DF_tbl_Roles_sysIsSystem] DEFAULT ((0)) NOT NULL,
    [sysIsDeleted]    BIT                 CONSTRAINT [DF_tbl_Roles_sysIsDeleted] DEFAULT ((0)) NOT NULL,
    [applicationName] NVARCHAR (15)       NULL,
    CONSTRAINT [PK__tbl_Role__CD98462A14245626] PRIMARY KEY CLUSTERED ([roleId] ASC),
    CONSTRAINT [CK_tbl_Roles_CanDelete] CHECK (NOT ([sysIsSystem]=(1) AND [sysIsDeleted]=(1)))
);





