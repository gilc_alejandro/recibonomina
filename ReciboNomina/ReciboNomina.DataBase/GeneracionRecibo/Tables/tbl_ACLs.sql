﻿CREATE TABLE [GeneracionRecibo].[tbl_ACLs] (
    [aclId]            UNIQUEIDENTIFIER CONSTRAINT [NEW_ID] DEFAULT (newid()) NOT NULL,
    [aclPrincipalId]   UNIQUEIDENTIFIER NOT NULL,
    [aclPrincipalType] CHAR (1)         NOT NULL,
    [aclSecurableId]   UNIQUEIDENTIFIER NOT NULL,
    [aclSecurableType] CHAR (1)         NOT NULL,
    [permissionId]     INT              NOT NULL,
    [aclIsGranted]     BIT              NOT NULL,
    [aclIsSystem]      BIT              CONSTRAINT [DF_tbl_ACLs_aclIsSystem] DEFAULT ((1)) NOT NULL,
    [sysCreationDate]  DATETIME         CONSTRAINT [DF_tbl_ACLs_sysCreationDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysCreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [sysModifyDate]    DATETIME         CONSTRAINT [DF_tbl_ACLs_sysModifyDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [sysModifiedBy]    UNIQUEIDENTIFIER NOT NULL,
    [sysIsDeleted]     BIT              CONSTRAINT [DF_tbl_ACLs_sysIsDeleted] DEFAULT ((0)) NOT NULL,
    [sysIsSystem]      BIT              CONSTRAINT [DF_tbl_ACLs_sysIsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_ACLs] PRIMARY KEY CLUSTERED ([aclId] ASC),
    CONSTRAINT [CK_tbl_ACLs_CanDelete] CHECK (NOT ([sysIsSystem]=(1) AND [sysIsDeleted]=(1))),
    CONSTRAINT [CK_tbl_ACLs_PrincipalsTypes] CHECK ([aclPrincipalType]='U' OR [aclPrincipalType]='R'),
    CONSTRAINT [IX_tbl_ACLs] UNIQUE NONCLUSTERED ([aclPrincipalId] ASC, [aclPrincipalType] ASC, [aclSecurableId] ASC, [aclSecurableType] ASC, [permissionId] ASC, [aclIsSystem] ASC)
);



