﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entrepidus.Data;
using System.Data;
using ReciboNomina.BODT;
using System.Data.SqlClient;

namespace ReciboNomina.DataManager
{
    public class DataAccessDataManager : AuditDataManager<Item, int, int>
    {
        #region Variables
        private const string INPUT_USER = "@usuario";
        private const string INPUT_USER_NAME = "@userName";
        private const string INPUT_USER_ID = "@userId";
        private const string INPUT_VALIDATE_FUNCTIONALITY = "@ValidateFunctionality";
        private const string INPUT_COMPANY = "@compania";
        private const string INPUT_EMPLOYEE = "@trabajador";
        private const string INPUT_TYPE = "@tipo";
        #endregion Variables

        #region Fields
        private const string FIELD_COMPANIA = "compania";
        private const string FIELD_NOMBRE_COMPANIA = "nombre_cia";
        private const string FIELD_TIPO_NOMINA = "tipo_nomina";
        private const string FIELD_TIPO_NOMINA_DESCRIPCION = "descripcion";
        private const string FIELD_ROLE_ID = "roleId";
        private const string FIELD_ROLE_NAME = "roleName";
        private const string FIELD_CLASE_NOMINA = "clase_nomina";
        private const string FIELD_CLASE_NOMINA_DESCRIPCION = "descripcion";
        #endregion Fields

        #region Store Procedures
        private const string USP_MOSTRAR_COMPANIAS = "[GeneracionRecibo].[usp_MostrarCompanias]";
        private const string USP_AUTHENTICATE_USER = "[GeneracionRecibo].[usp_Authenticate]";
        private const string USP_MOSTRAR_TIPO_NOMINA = "[GeneracionRecibo].[usp_mostrarTipoNomina]";
        private const string USP_GET_ROLES_FOR_USER = "[GeneracionRecibo].[usp_GetRolesForUser]";
        private const string USP_GET_VALIDATE_LIST_USER_AUTHORIZATION = "GeneracionRecibo.usp_GetValidateListUserAuthorization";
        private const string USP_GET_CLASE_NOMINA = "[GeneracionRecibo].[usp_clases_nomina]";
        private const string USP_GET_TIPO_NOMINA = "[GeneracionRecibo].[usp_mostrarTipoNomina]";
        private const string USP_SET_PAYROLL = "[dbo].[SP_Fpv_Calcula_Repnom]";
        private const string USP_GET_EMPLOYEES = "[GeneracionRecibo].[usp_mostrarTrabajadoresRecGenerados]";
        private const string USP_GET_INVOICES = "[GeneracionRecibo].[usp_mostrarENRecibo]";
        private const string USP_GET_INVOICES_JUST_HEADER = "[GeneracionRecibo].usp_GetRecNomHdr";
        private const string USP_GET_INVOICES_DETAILS = "[GeneracionRecibo].[usp_mostrarDetalleRecibo]";
        private const string USP_SAVE_EMPLOYEE_INVOICE = "[GeneracionRecibo].[usp_SaveEmployeeInvoice]";
        private const string USP_GET_GENERATED_INVOICES_BY_ID = "[GeneracionRecibo].usp_GetGeneratedInvoicesById";
        private const string USP_GET_GENERATED_INVOICES_BY_ID_2 = "[GeneracionRecibo].[usp_GetPayrollInvoicesById]";
        private const string USP_SET_INVOICES_AS_SENT = "[GeneracionRecibo].[usp_SetInvoiceAsSent]";
        #endregion Store Procedures

        public DataAccessDataManager()
        {
            this.AddProcedure = "xxx";
            this.ModifyProcedure = "xxx";
            this.GetItemProcedure = "xxx";
            this.GetItemsProcedure = "xxx";
            this.DeleteProcedure = "xxx";
        }

        protected override void CreateAddParameters(Item item, int userId, DbCommand cmd)
        {
            cmd.Parameters.Clear();
        }

        protected override void CreateRemoveParameters(int id, int userId, DbCommand cmd)
        {
            cmd.Parameters.Clear();
        }

        protected override void CreateModifyParameters(Item item, int userId, DbCommand cmd)
        {
            cmd.Parameters.Clear();
        }

        protected override void CreateGetItemParameters(int id, DbCommand cmd, params IFilter[] filters)
        {
            cmd.Parameters.Clear();
        }

        protected override void CreateGetItemsParameters(int pageId, int rowsByPage, DbCommand cmd, params IFilter[] filters)
        {
            cmd.Parameters.Clear();
        }

        protected override IEnumerable<DataMapping> GetDataMappings()
        {
            HashSet<DataMapping> mappings = new HashSet<DataMapping>();
            return mappings;
        }

        protected override IEnumerable<DataMapping> GetPrimaryKeyDataMappings()
        {
            HashSet<DataMapping> mappings = new HashSet<DataMapping>();
            return mappings;
        }

        public DataTable AuthenticateUser(string userName, string password)
        {
            DataSet authenticateUser = new DataSet();
            DataTable ret = new DataTable();
            System.Data.Common.DbCommand cmd = null;

            try
            {
                cmd = Database.GetStoredProcCommand(USP_AUTHENTICATE_USER);
                Database.AddInParameter(cmd, "@userName", DbType.String, userName);
                Database.AddInParameter(cmd, "@userPassword", DbType.String, password);

                authenticateUser = Database.ExecuteDataSet(cmd);
                ret = authenticateUser.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }

            return ret;
        }

        public IEnumerable<ComboItem> GetCompanies(string user)
        {
            List<ComboItem> list = new List<ComboItem>();
            DbCommand cmd = Database.GetStoredProcCommand(USP_MOSTRAR_COMPANIAS);
            IDataReader dr = null;
            try
            {
                Database.AddInParameter(cmd, INPUT_USER, DbType.String, user);
                dr = Database.ExecuteReader(cmd);
                while (dr.Read())
                {
                    ComboItem item = new ComboItem();
                    item.Value = dr[FIELD_COMPANIA].ToString().Trim();
                    item.Text = dr[FIELD_NOMBRE_COMPANIA].ToString();
                    list.Add(item);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                {
                    cmd.Connection.Close();
                }
            }
            return list;
        }

        public IEnumerable<ComboItem> GetPayRollList()
        {
            List<ComboItem> list = new List<ComboItem>();
            DbCommand cmd = Database.GetStoredProcCommand(USP_MOSTRAR_TIPO_NOMINA);
            IDataReader dr = null;
            try
            {
                dr = Database.ExecuteReader(cmd);
                while (dr.Read())
                {
                    ComboItem item = new ComboItem();
                    item.Value = dr[FIELD_TIPO_NOMINA].ToString();
                    item.Text = dr[FIELD_TIPO_NOMINA_DESCRIPCION].ToString();
                    list.Add(item);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                {
                    cmd.Connection.Close();
                }
            }
            return list;
        }

        public IEnumerable<ComboItem> GetRolesIdForUserName(string userName)
        {
            List<ComboItem> list = new List<ComboItem>();

            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_ROLES_FOR_USER);
            IDataReader dr = null;
            try
            {
                Database.AddInParameter(cmd, INPUT_USER_NAME, DbType.String, userName);
                dr = Database.ExecuteReader(cmd);
                while (dr.Read())
                {
                    ComboItem item = new ComboItem();
                    item.Value = dr[FIELD_ROLE_ID].ToString();
                    item.Text = dr[FIELD_ROLE_NAME].ToString();
                    list.Add(item);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (dr != null)
                    dr.Close();

                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }

            return list;
        }

        public DataTable GetListValidUserAuthorization(Guid userId, bool validateFunctionality = true)
        {
            DataTable dt = new DataTable();

            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_VALIDATE_LIST_USER_AUTHORIZATION);
            try
            {
                Database.AddInParameter(cmd, INPUT_USER_ID, DbType.Guid, userId);
                Database.AddInParameter(cmd, INPUT_VALIDATE_FUNCTIONALITY, DbType.Boolean, validateFunctionality);
                DataSet ds = Database.ExecuteDataSet(cmd);

                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }

            return dt;
        }

        public IEnumerable<ComboItem> GetClassOfPayroll()
        {
            List<ComboItem> list = new List<ComboItem>();

            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_CLASE_NOMINA);
            IDataReader dr = null;
            try
            {
                dr = Database.ExecuteReader(cmd);
                while (dr.Read())
                {
                    ComboItem item = new ComboItem();
                    item.Value = dr[FIELD_CLASE_NOMINA].ToString();
                    item.Text = dr[FIELD_CLASE_NOMINA_DESCRIPCION].ToString();
                    list.Add(item);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (dr != null)
                    dr.Close();

                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }

            return list;
        }

        public IEnumerable<ComboItem> GetTypeOfPayroll()
        {
            List<ComboItem> list = new List<ComboItem>();

            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_TIPO_NOMINA);
            IDataReader dr = null;
            try
            {
                dr = Database.ExecuteReader(cmd);
                while (dr.Read())
                {
                    ComboItem item = new ComboItem();
                    item.Value = dr[FIELD_TIPO_NOMINA].ToString();
                    item.Text = dr[FIELD_TIPO_NOMINA_DESCRIPCION].ToString();
                    list.Add(item);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (dr != null)
                    dr.Close();

                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }

            return list;
        }

        public long GenerateInvoiceClassOfPayroll(params IFilter[] filters)
        {
            long ret = 0;
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_SET_PAYROLL);
            if (filters != null && filters.Length > 0)
            {
                foreach (var item in filters)
                {
                    Database.AddInParameter(cmd, item.Name, item.DbType, item.Value);
                }
            }
            try
            {
                ret = Database.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
            return ret;
        }

        public long CalculateInvoiceByEmployee(params IFilter[] filters)
        {
            long ret = 0;
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_SET_PAYROLL);
            if (filters != null && filters.Length > 0)
            {
                foreach (var item in filters)
                {
                    Database.AddInParameter(cmd, item.Name, item.DbType, item.Value);
                }
            }
            try
            {
                ret = Database.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
            return ret;
        }

        public DataTable GetInvoiceEmployeeByCompany(string company)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_EMPLOYEES);
            try
            {
                Database.AddInParameter(cmd, INPUT_COMPANY, DbType.String, company);
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public DataTable GetPayrollInvoicesByEmployee(string company, string employee, string type, Guid? generatedInvooiceId)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_INVOICES);
            try
            {
                Database.AddInParameter(cmd, INPUT_COMPANY, DbType.String, company);
                Database.AddInParameter(cmd, INPUT_EMPLOYEE, DbType.String, employee);
                Database.AddInParameter(cmd, INPUT_TYPE, DbType.String, type);
                Database.AddInParameter(cmd, "generatedInvoiceId", DbType.Guid, generatedInvooiceId);
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public DataTable GetPayrollInvoicesByEmployeeDetails(params IFilter[] filters)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_INVOICES_DETAILS);
            try
            {
                if (filters != null && filters.Length > 0)
                {
                    foreach (var item in filters)
                    {
                        Database.AddInParameter(cmd, item.Name, item.DbType, item.Value);
                    }
                }
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public DataTable SaveEmployeeInvoice(Guid userId, params IFilter[] filters)
        {
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_SAVE_EMPLOYEE_INVOICE);
            DataTable dtParameters = new DataTable("dtParameters");
            DataTable dtEmployeeInvoices = new DataTable("EmployeeInvoice");
            try
            {
                if (filters != null && filters.Length > 0)
                {
                    foreach (var item in filters)
                    {
                        dtParameters = (DataTable)item.Value;
                    }
                }

                SqlParameter paramT = new SqlParameter();
                paramT.ParameterName = "employeeInvoice";
                paramT.SqlDbType = System.Data.SqlDbType.Structured;
                paramT.Value = dtParameters;
                paramT.TypeName = "[GeneracionRecibo].[EmployeeInvoice]";
                cmd.Parameters.Add(paramT);
                Database.AddInParameter(cmd, "userId", DbType.Guid, userId);

                dtEmployeeInvoices.Load(Database.ExecuteReader(cmd));
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                {
                    cmd.Connection.Close();
                }
            }
            return dtEmployeeInvoices;
        }

        public DataTable GetGeneratedInvoicesByEmployeeById(Guid? id)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_GENERATED_INVOICES_BY_ID);
            try
            {
                Database.AddInParameter(cmd, "id", DbType.Guid, id);
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public DataTable GetGeneratedInvoicesByEmployeeById_2(Guid? id)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_GENERATED_INVOICES_BY_ID_2);
            try
            {
                Database.AddInParameter(cmd, "id", DbType.Guid, id);
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public DataTable GetHeaderInvoices(string company, string employee, string type)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand(USP_GET_INVOICES_JUST_HEADER);
            try
            {
                Database.AddInParameter(cmd, INPUT_COMPANY, DbType.String, company);
                Database.AddInParameter(cmd, INPUT_EMPLOYEE, DbType.String, employee);
                Database.AddInParameter(cmd, INPUT_TYPE, DbType.String, type);
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public Guid GetInvoicesDidNotSend()
        {
            Guid id = Guid.Empty;
            System.Data.Common.DbCommand cmd = null;
            try
            {
                cmd = Database.GetStoredProcCommand("[GeneracionRecibo].[usp_GetInvoicesDidNotSend]");
                string x = Database.ExecuteScalar(cmd).ToString();
                bool tryParse = Guid.TryParse(x.ToString(), out id);
            }
            catch 
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return id;
        }

        public DataTable GetEmployeesDidNotSendInvoices(Guid id)
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = null;
            try
            {
                cmd = Database.GetStoredProcCommand("[GeneracionRecibo].[usp_GetPayrollInvoicesToSendEmail]");
                Database.AddInParameter(cmd, "id", DbType.Guid, id);
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }

        public bool SetInvoiceAsSent(Guid id)
        {
            bool ret = false;
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand("[GeneracionRecibo].[usp_SetInvoiceAsSent]");

            try
            {
                Database.AddInParameter(cmd, "id", DbType.Guid, id);
                Database.ExecuteNonQuery(cmd);
                ret = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return ret;
        }

        public bool InvoiceSave (Guid? id, string path, string employeeFullName)
        {
            bool ret = false;
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand("GeneracionRecibo.usp_SavePath");

            try
            {
                Database.AddInParameter(cmd, "id", DbType.Guid, id);
                Database.AddInParameter(cmd, "invoicePath", DbType.String, path);
                Database.AddInParameter(cmd, "employeeFullName", DbType.String, employeeFullName);
                Database.ExecuteNonQuery(cmd);
                ret = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return ret;
        }

        public DataTable GetInvoicesDidNotSendDetails()
        {
            DataTable dt = new DataTable();
            System.Data.Common.DbCommand cmd = Database.GetStoredProcCommand("[GeneracionRecibo].[usp_GetInvoicesDidNotSendDetails]");
            try
            {
                DataSet ds = Database.ExecuteDataSet(cmd);
                dt = ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                // Close Connection
                if (cmd != null && cmd.Connection != null)
                    cmd.Connection.Close();
            }
            return dt;
        }
    }
}
