﻿using Microsoft.Reporting.WebForms;
using ReciboNomina.BODT;
using ReciboNomina.BusinessManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPNETMVC_SSRS_Demo.Reports
{
    public partial class ReportTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ReportParameter[] param = new ReportParameter[1];
                    param[0] = new ReportParameter("id", Request["id"].ToString());
                    var uri = new Uri("http://localhost/ReportServer");

                    rvSiteMapping.Height = Unit.Pixel(800);
                    rvSiteMapping.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    rvSiteMapping.ServerReport.ReportServerUrl = uri; 
                    rvSiteMapping.ServerReport.ReportPath = String.Format("/{0}", Request["ReportName"].ToString());
                    rvSiteMapping.ServerReport.SetParameters(param);
                    rvSiteMapping.ServerReport.Refresh();
                }
                catch (Exception ex)
                {
                    
                }
            }
        }
    }
}