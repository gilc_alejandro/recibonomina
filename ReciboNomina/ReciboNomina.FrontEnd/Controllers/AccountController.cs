﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ReciboNomina.FrontEnd.Models;
using ReciboNomina.BusinessManager;
using System.Data;
using ReciboNomina.BODT;
using System.Collections.Generic;
using System.Web.Routing;
using System.Web.Security;
using ReciboNomina.FrontEnd.Utilities;
using System.Text;

namespace ReciboNomina.FrontEnd.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {

        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            List<ComboItem> sourceList = new List<ComboItem>();
            LoginViewModel loginVM = new LoginViewModel();
            sourceList.Add(new ComboItem("", ReciboNomina.BODT.Resources.Resources.Common_Select));
            loginVM.SourceList = sourceList;

            return View(loginVM);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            #region Identity        
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            //var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            /*
               switch (result)
               {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(model);
               }
            */
            #endregion Identity
            DataAccessManager auth = new DataAccessManager();
            DataTable dt = new DataTable();
            dt = auth.AuthenticateUser(model.UserName, model.Password);
            if (dt.Rows.Count > 0)
            {
                if (model.UserName.Equals(dt.Rows[0].ItemArray[1].ToString()) && model.Password.Equals(dt.Rows[0].ItemArray[2].ToString()))
                {
                    //Get Roles by User

                    model.Roles = auth.GetRolesByUser(model.UserName).ToList();

                    System.Text.StringBuilder rolesInfo = new System.Text.StringBuilder();
                    string separator = "|";
                    bool firstIteration = true;
                    int version = 1;
                    foreach (var item in model.Roles)
                    {
                        if (firstIteration)
                            firstIteration = false;
                        else
                            rolesInfo.AppendFormat(separator);

                        rolesInfo.AppendFormat("{0}:{1}", item.Value, item.Text);
                    }

                    // Get Access Permisions
                    DataTable actionListUser = auth.GetListValidUserAuthorization(Guid.Parse(dt.Rows[0].ItemArray[0].ToString()));
                    firstIteration = true;
                    System.Text.StringBuilder actionsInfo = new StringBuilder();

                    foreach (DataRow item in actionListUser.Rows)
                    {
                        if (firstIteration)
                            firstIteration = false;
                        else
                            actionsInfo.AppendFormat(separator);

                        //actionsInfo.AppendFormat("{0}:{1}:{2}", item.Id, item.SysControllerName, item.SysActionName);
                        actionsInfo.AppendFormat("{0}:{1}", item[1], item[2]);
                    }

                    // Create Ticket
                    string userData = string.Concat(dt.Rows[0].ItemArray[0].ToString(), "||", dt.Rows[0].ItemArray[3].ToString());
                    string ticketData;
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(version, dt.Rows[0].ItemArray[3].ToString(), DateTime.Now,
                        DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes), false, userData, FormsAuthentication.FormsCookiePath);

                    ticketData = FormsAuthentication.Encrypt(ticket);

                    HttpCookie securityCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
                    securityCookie.Domain = FormsAuthentication.CookieDomain;
                    securityCookie.Value = ticketData;
                    Response.Cookies.Add(securityCookie);

                    // Create Cookie Additional Info
                    string valuesCookieUser = string.Concat("#", "#", actionsInfo);
                    HttpCookie cookieUserInfo = new HttpCookie("UserInfoCookie");
                    cookieUserInfo.Domain = FormsAuthentication.CookieDomain;
                    cookieUserInfo.Value = Entrepidus.Core.Utilities.FunctionsManager.Base64EncodeUTF8(valuesCookieUser);
                    //cookieUserInfo.Values.Add("AppId", Entrepidus.Core.Utilities.FunctionsManager.Base64EncodeUTF8(currentApplicationId));
                    //cookieUserInfo.Values.Add("Access", Entrepidus.Core.Utilities.FunctionsManager.Base64EncodeUTF8(actionsInfo.ToString()));
                    HttpContext.Response.AppendCookie(cookieUserInfo);

                    return RedirectToAction("Read", new { userName = dt.Rows[0].ItemArray[3].ToString() });
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }
        }
        //
        // GET: /Account/SelectCompany
        [Role("Read")]
        public ActionResult Read(string userName)
        {
            Recibo invoicePayroll = new Recibo();
            DataAccessManager dataAccessM = new DataAccessManager();
            List<ComboItem> companyList = new List<ComboItem>();
            companyList.Add(new ComboItem("0", ReciboNomina.BODT.Resources.Resources.Common_Select));

            foreach (var company in dataAccessM.GetCompanies(userName))
            {
                companyList.Add(new ComboItem(company.Value, company.Text));
            }
            invoicePayroll.CompanyList.AddRange(companyList);
            invoicePayroll.SESA = userName;
            return View(invoicePayroll);
        }

        //
        // POST: /Account/Read
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Read(Recibo model)
        {
            ModelState.Remove("Year");
            ModelState.Remove("Period");
            ModelState.Remove("ClassOfPayroll");
            ModelState.Remove("TypeOfPayroll");
            ModelState.Remove("SelectedTypeOfInvoice");
            ModelState.Remove("SelectedGenerateBy");
            if (!ModelState.IsValid)
            {
                Recibo invoicePayroll = new Recibo();
                DataAccessManager dataAccessM = new DataAccessManager();
                List<ComboItem> companyList = new List<ComboItem>();
                companyList.Add(new ComboItem("", ReciboNomina.BODT.Resources.Resources.Common_Select));

                foreach (var company in dataAccessM.GetCompanies(model.SESA))
                {
                    companyList.Add(new ComboItem(company.Value, company.Text));
                }
                invoicePayroll.CompanyList.AddRange(companyList);
                ModelState.AddModelError(String.Empty, ModelState.Values.FirstOrDefault(x => x.Errors.Count > 0).Errors.FirstOrDefault().ErrorMessage);
                return View(model);
            }
            return RedirectToAction("Read", "Home", new { userName = model.SESA, company = model.Company });
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }


        //
        // POST: /Account/LogOff
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            HttpContext.Session.RemoveAll();

            var urlToRemove = Url.Action("Read", "Home");
            Response.RemoveOutputCacheItem(urlToRemove);

            return RedirectToAction("Read", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}