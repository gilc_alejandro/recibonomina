﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ReciboNomina.BusinessManager;
using ReciboNomina.BODT;
using ReciboNomina.FrontEnd.Utilities;
using Microsoft.Reporting.WebForms;
using System.IO;
using Entrepidus.Data;
using System.Data;
using System.Linq;
using static ReciboNomina.BODT.Recibo;

namespace ReciboNomina.FrontEnd.Controllers
{
    public class HomeController : Controller
    {
        [Role("Read")]
        public ActionResult Read(string userName, string company)
        {
            Recibo recibo = new Recibo();
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(company))
            {
                GetInputsForGenerateInvoice(ref recibo);
                recibo.Company = company;
                recibo.SESA = userName;
                Session["User"] = userName;
                Session["Company"] = company;
            }
            else
            {
                ModelState.AddModelError(string.Empty, "No se puede procesar el recibo, falta el usuario o la compañía elegida");
            }
            
            return View(recibo);
        }

        [Role("Read")]
        [HttpPost]
        public ActionResult Read(Recibo recibo)
        {
            if (!ModelState.IsValid)
            {
                GetInputsForGenerateInvoice(ref recibo);
                ModelState.AddModelError(string.Empty, string.Empty);
                return View(recibo);
            }
            string session = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            DataAccessManager dataAccessM = new DataAccessManager();
            DataTable ret = new DataTable();
            IFilter[] filters = null;
            filters = new Entrepidus.Data.Filter[13];
            filters[0] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Compania", Value = recibo.Company };
            filters[1] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_ClaseNomina", Value = recibo.ClassOfPayroll };
            filters[2] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_TipoNomina", Value = recibo.TypeOfPayroll };
            filters[3] = new Entrepidus.Data.Filter() { DbType = DbType.Int32, Name = "i_Anio", Value = recibo.Year };
            filters[4] = new Entrepidus.Data.Filter() { DbType = DbType.Int32, Name = "i_Periodo", Value = recibo.Period };
            filters[5] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato1", Value = string.Empty };
            filters[6] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato2", Value = string.Empty };
            filters[7] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato3", Value = string.Empty };
            filters[8] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato4", Value = string.Empty };
            filters[9] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Operacion", Value = GrupoJS.Generales.extraerValordeconfiguracion("Operacion") };
            filters[10] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Usuario", Value = recibo.SESA.Trim() };
            filters[11] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_id_Session", Value = session };
            filters[12] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_trabajador", Value =  recibo.Employee == string.Empty ? string.Empty : recibo.Employee };

            try
            {
                //Calculate Invoice
                ret = dataAccessM.GenerateClassOfPayrollInvoice(filters);
                Guid userId = AppCookies.UserId;
                Guid? lastGeneratedInvoiceId = null;
                DataTable getPayrollInvoicesByEmployeeDT = new DataTable();
                
                foreach (DataRow row in ret.Rows)
                {
                    filters[12].Value = row.ItemArray[0].ToString();
                    dataAccessM.CalculateInvoiceByEmployee(filters);
                    getPayrollInvoicesByEmployeeDT = DataAccessManager.GetPayrollInvoicesByEmployee(filters, lastGeneratedInvoiceId);
                    int count = getPayrollInvoicesByEmployeeDT.Rows[0].ItemArray.Length - 1;
                    lastGeneratedInvoiceId = Guid.Parse(getPayrollInvoicesByEmployeeDT.Rows[0].ItemArray[count].ToString());

                    if (recibo.SelectedTypeOfInvoice == ReciboNomina.BODT.Recibo.TypeOfInvoice.ByEmail)
                    {
                        //Generate Invoice in PDF Format
                        GenerateInvoiceInPDFFormat(getPayrollInvoicesByEmployeeDT, lastGeneratedInvoiceId);
                    }
                }

                if (recibo.SelectedTypeOfInvoice == ReciboNomina.BODT.Recibo.TypeOfInvoice.ByScreen)
                {
                    TempData["InvoicesByEmployeeDT"] = dataAccessM.GetGeneratedInvoicesByEmployeeById_2(lastGeneratedInvoiceId); ;
                    string basePath_2 = GrupoJS.Generales.extraerValordeconfiguracion("BaseInvoice_2");
                    basePath_2 = Server.MapPath(basePath_2);
                    return RedirectToAction("GenerateInvoiceInScreen", new { reportPath = basePath_2,  id = lastGeneratedInvoiceId}); 
                }
                recibo.InvoicesList = dataAccessM.GetInvoicesDidNotSendDetails();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            GetInputsForGenerateInvoice(ref recibo);
            return View("GenerateInvoiceInPDFFormat", recibo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult GetEmployess(Recibo recibo)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, string.Empty);
                return PartialView("_GetEmployess", recibo);
            }
            Recibo employeeList = new Recibo();
            DataTable ret = new DataTable();
            DataAccessManager dataAccessM = new DataAccessManager();
            string session = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            IFilter[] filters = null;
            filters = new Entrepidus.Data.Filter[13];
            filters[0] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Compania", Value = recibo.Company };
            filters[1] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_ClaseNomina", Value = recibo.ClassOfPayroll };
            filters[2] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_TipoNomina", Value = recibo.TypeOfPayroll };
            filters[3] = new Entrepidus.Data.Filter() { DbType = DbType.Int32, Name = "i_Anio", Value = recibo.Year };
            filters[4] = new Entrepidus.Data.Filter() { DbType = DbType.Int32, Name = "i_Periodo", Value = recibo.Period };
            filters[5] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato1", Value = string.Empty };
            filters[6] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato2", Value = string.Empty };
            filters[7] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato3", Value = string.Empty };
            filters[8] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Dato4", Value = string.Empty };
            filters[9] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Operacion", Value = GrupoJS.Generales.extraerValordeconfiguracion("Operacion") };
            filters[10] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_Usuario", Value = recibo.SESA.Trim() };
            filters[11] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_id_Session", Value = session };
            filters[12] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "i_trabajador", Value = string.Empty };

            //Calculate Invoice
            ret = dataAccessM.GenerateClassOfPayrollInvoice(filters);

            List<ComboItem> employeesList = new List<ComboItem>();
            employeesList.Add(new ComboItem("", ReciboNomina.BODT.Resources.Resources.Common_Select));
            foreach (DataRow employee in ret.Rows)
            {
                employeesList.Add(new ComboItem(employee.ItemArray[0].ToString().Trim(), employee.ItemArray[3].ToString().Trim()));
            }
            recibo.EmployeeList.AddRange(employeesList);
            return PartialView("_GetEmployess", recibo);
        }

        public ActionResult GenerateInvoiceInScreen(string reportPath, Guid id)
        {
            Warning[] warnings = null;
            string[] streamIds = null;
            string mimeType = string.Empty, encoding = string.Empty, extension = string.Empty, filetype = string.Empty, userName = string.Empty;
            string destinationFolder = GrupoJS.Generales.extraerValordeconfiguracion("DestinationInvoicesPath");
            var path = Server.MapPath(destinationFolder);
            Stream stream = null;
            var _dt = TempData["InvoicesByEmployeeDT"];
            DataTable dt = (DataTable)_dt;
            try
            {
                ReportViewer viewer = new ReportViewer();

                viewer.LocalReport.ReportPath = reportPath;
                viewer.ProcessingMode = ProcessingMode.Local;

                ReportParameter[] param = new ReportParameter[1];
                param[0] = new ReportParameter("id", id.ToString());

                ReportDataSource rDS = new ReportDataSource("ReciboNominaHeaderById", dt);
                viewer.LocalReport.DataSources.Add(rDS);
                viewer.LocalReport.SetParameters(param);
                viewer.LocalReport.Refresh();

                byte[] bytes = viewer.LocalReport.Render("PDF", null,
                out mimeType, out encoding, out extension, out streamIds, out warnings);

                stream = new MemoryStream(bytes);

                using (FileStream file = new FileStream(path + "\\" + "ReciboNomina" + dt.Rows[0].ItemArray[5].ToString().Trim() + ".pdf", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(bytes, 0, bytes.Length);
                    stream.Close();
                    //ret = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new FileStreamResult(stream, "application/pdf");
        }

        //public ActionResult GenerateInvoiceInScreen(Guid id, int? width, int? height)
        //{
        //    ReportInfo rptInfo = new ReportInfo();
        //    try
        //    {
        //        rptInfo = new ReportInfo
        //        {
        //            ReportName = "ReciboNomina_2",
        //            ReportURL = String.Format("../../Reports/ReportTemplate.aspx?ReportName={0}&height={1}&id={2}", "ReciboNomina_2", height, id),
        //            Width = width,
        //            Height = height
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return View("ViewEmployeeInvoice", rptInfo);
        //}

        private bool GenerateInvoiceInPDFFormat(DataTable dt, Guid? id)
        {
            Warning[] warnings = null;
            string[] streamIds = null;
            string mimeType = string.Empty,encoding = string.Empty, extension = string.Empty, filetype = string.Empty, userName = string.Empty;
            bool ret = false;
            string destinationFolder = GrupoJS.Generales.extraerValordeconfiguracion("DestinationInvoicesPath");
            var path = Server.MapPath(destinationFolder);
            string reportPath = GrupoJS.Generales.extraerValordeconfiguracion("BaseInvoice");//BaseInvoiceFewDescriptions
            reportPath = Server.MapPath(reportPath);
            DataAccessManager dataAccessM = new DataAccessManager();
            DataTable dtDetails = new DataTable();

            try
            {
                IFilter[] filters = null;
                filters = new Entrepidus.Data.Filter[2];
                filters[0] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "compania", Value = dt.Rows[0].ItemArray[0].ToString().Trim() };
                filters[1] = new Entrepidus.Data.Filter() { DbType = DbType.String, Name = "trabajador", Value = dt.Rows[0].ItemArray[1].ToString().Trim() };

                dtDetails = DataAccessManager.GetPayrollInvoicesByEmployeeDetails(filters);

                if (dtDetails.Rows.Count < 3)
                {
                    reportPath = GrupoJS.Generales.extraerValordeconfiguracion("BaseInvoiceFewDescriptions");
                    reportPath = Server.MapPath(reportPath);
                }

                ReportViewer viewer = new ReportViewer();

                viewer.LocalReport.ReportPath = reportPath;
                viewer.ProcessingMode = ProcessingMode.Local;

                ReportParameter[] param = new ReportParameter[3];
                param[0] = new ReportParameter("compania", dt.Rows[0].ItemArray[0].ToString().Trim());
                param[1] = new ReportParameter("trabajador", dt.Rows[0].ItemArray[1].ToString().Trim());
                param[2] = new ReportParameter("tipo", "T");

                ReportDataSource rDS = new ReportDataSource("ReciboNominaDataSet", dt);

               
                ReportDataSource rDS2 = new ReportDataSource("ReciboNominaDetailsDataset", dtDetails);

                viewer.LocalReport.DataSources.Add(rDS);
                viewer.LocalReport.DataSources.Add(rDS2);
                viewer.LocalReport.SetParameters(param);
                viewer.LocalReport.Refresh();

                byte[] bytes = viewer.LocalReport.Render("PDF", null,
                out mimeType, out encoding, out extension, out streamIds, out warnings);

                Stream stream = new MemoryStream(bytes);

                using (FileStream file = new FileStream(path += "\\" 
                    + DateTime.Parse(dt.Rows[0].ItemArray[7].ToString().Trim()).Year.ToString() 
                    + DateTime.Parse(dt.Rows[0].ItemArray[7].ToString().Trim()).Month.ToString("00") 
                    + DateTime.Parse(dt.Rows[0].ItemArray[7].ToString().Trim()).Day.ToString()
                    + dt.Rows[0].ItemArray[5].ToString().Trim()
                    + dt.Rows[0].ItemArray[1].ToString().Trim()
                    + " " + dt.Rows[0].ItemArray[22].ToString().Trim()
                    + ".pdf", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(bytes, 0, bytes.Length);
                    stream.Close();
                    dataAccessM.InvoiceSave(id, path, dt.Rows[0].ItemArray[22].ToString().Trim());
                    ret = true;
                }
            }
            catch (Microsoft.Reporting.WebForms.LocalProcessingException ex)
            {
                throw;
            }
            return ret;
        }

        public ActionResult GetAllPayrollInvoices()
        {
            Recibo recibo = new Recibo();
            DataAccessManager dataAccessM = new DataAccessManager();
            recibo.InvoicesList = dataAccessM.GetInvoicesDidNotSendDetails();
            return View("GenerateInvoiceInPDFFormat", recibo);
        }

        public PartialViewResult Grid(Guid? generatedInvoiceId)
        {
            Recibo recibo = new Recibo();
            DataTable getPayrollInvoicesByEmployeeDT = new DataTable();
            DataAccessManager dataAccessM = new DataAccessManager();
            recibo.InvoicesList = getPayrollInvoicesByEmployeeDT = dataAccessM.GetGeneratedInvoicesByEmployeeById(generatedInvoiceId);
            return PartialView("_Grid", recibo);
        }

        #region PrivateMethods
        private Recibo GetInputsForGenerateInvoice(ref Recibo recibo)
        {
            DataAccessManager dataAccessM = new DataAccessManager();
            //Get Clase de Nomina
            List<ComboItem> claseNominaList = new List<ComboItem>();
            claseNominaList.Add(new ComboItem("", ReciboNomina.BODT.Resources.Resources.Common_Select));
            foreach (var claseNomina in dataAccessM.GetClassOfPayroll())
            {
                claseNominaList.Add(new ComboItem(claseNomina.Value, claseNomina.Text));
            }
            recibo.ClassOfPayrollList.AddRange(claseNominaList);

            //Get Tipo de Nomina
            List<ComboItem> tipoNominaList = new List<ComboItem>();
            tipoNominaList.Add(new ComboItem("", ReciboNomina.BODT.Resources.Resources.Common_Select));
            foreach (var tipoNomina in dataAccessM.GetTypeOfPayroll())
            {
                tipoNominaList.Add(new ComboItem(tipoNomina.Value, tipoNomina.Text));
            }
            recibo.TypeOfPayrollList.AddRange(tipoNominaList);
            return recibo;
        }
        #endregion
    }
}