﻿using Entrepidus.Core.Utilities;
using System;
using System.Web;
using System.Web.Mvc;


namespace ReciboNomina.FrontEnd.Utilities
{
    public class Role : AuthorizeAttribute
    {
        private const string USER_INFO_COOKIE = "UserInfoCookie";

        private string _authRoles;
        public string AuthRoles
        {
            get { return _authRoles; }
            set { _authRoles = value; }
        }

        private bool IsMobile = false;
        private string Action;

        public Role(string _Action = "")
        {
            IsMobile = false;
            Action = _Action;
        }

        public Role(bool isMobile)
        {
            IsMobile = isMobile;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (String.IsNullOrEmpty(Action))
                Action = filterContext.ActionDescriptor.ActionName;


            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
               // base.OnAuthorization(filterContext);

                if (!Validate(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, Action))
                {
                    if (IsMobile)
                    {
                        throw new NotImplementedException();
                        //filterContext.Result = new RedirectResult("~/Mobile/NotAuthorized");
                    }
                    else
                    {
                        string[] acceptedTypes = filterContext.HttpContext.Request.AcceptTypes;
                        foreach (string type in acceptedTypes)
                        {
                            if (type.Contains("html") || type.Contains("*/*"))
                            {
                                filterContext.Result = new PartialViewResult { ViewName = "_NotAuthorized" }; 
                                break;
                            }
                            else if (type.Contains("javascript"))
                            {
                                filterContext.Result = new JsonResult { Data = new { success = false, message = "Access denied." }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (IsMobile)
                {
                    throw new NotImplementedException();
                    //filterContext.HttpContext.Response.Redirect("~/Mobile/Index?returnUrl=" +
                    //                            filterContext.HttpContext.Server.UrlEncode(filterContext.HttpContext.Request.RawUrl));
                }
                else
                    filterContext.Result = new HttpUnauthorizedResult(); //this will redirect to login page.
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            bool ret = false;

            try
            {
                string controller = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
                string action = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();

                ret = Role.Validate(controller, action);
            }
            catch
            {
                //TODO: Handle Exception
                ret = false;
            }

            return (ret);
        }

        public static bool Validate(string controller, string action)
        {
            bool ret = false;
            try
            {
                /*
                int userId = ((SCDMembership.SCDIdentity)HttpContext.Current.User.Identity).Id;
                
                 * SCDMembership.SCDMembershipProvider userProvider = (SCDMembership.SCDMembershipProvider)Membership.Provider;
                SCDMembership.SCDAuthorizationProvider authProvider = new SCDMembership.SCDAuthorizationProvider();
                
                ret = authProvider.ValidateUserAuthorization(userProvider.ApplicationId, userId, controller, action, true);
                 */
                ret = GetAccessPermisionFromUserInfoCookie(controller.ToLower(), action.ToLower());
            }
            catch
            {
                //TODO: Handle Exception
                ret = false;
            }
            return ret;
        }

        public static bool GetAccessPermisionFromUserInfoCookie(string controller, string action)
        {
            try
            {
                HttpCookie userInfoCookie = new HttpCookie(USER_INFO_COOKIE);
                userInfoCookie = HttpContext.Current.Request.Cookies[USER_INFO_COOKIE];
                string[] arrValues = FunctionsManager.Base64DecodeUTF8(userInfoCookie.Value).Split(char.Parse("#"));

                string target = string.Concat(controller, ":", action);
                string accessInfo = arrValues[2];

                string[] arrAccess = accessInfo.ToLower().Split(char.Parse("|"));
                bool has = Array.FindAll(arrAccess, s => s.Equals(target)).Length > 0;
                return has;
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidateUserAuthorizationDataCollector()
        {
            bool ret = false;

            if (HttpContext.Current.User.IsInRole("MobileOperator") || HttpContext.Current.User.IsInRole("DataCollector"))
                ret = true;

            return ret;
        }
    }
}