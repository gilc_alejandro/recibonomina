﻿using Entrepidus.Core.Utilities;
using System;
using System.Security.Authentication;
using System.Web;
using System.Web.Security;

namespace ReciboNomina.FrontEnd.Utilities
{
    public class AppCookies
    {
        private const string USER_INFO_COOKIE = "UserInfoCookie";

        public static string GetAppIdFromUserInfoCookie()
        {
            try
            {
                HttpCookie userInfoCookie = new HttpCookie(USER_INFO_COOKIE);
                userInfoCookie = HttpContext.Current.Request.Cookies[USER_INFO_COOKIE];
                string[] arrValues = FunctionsManager.Base64DecodeUTF8(userInfoCookie.Value).Split(char.Parse("#"));
                string appId = arrValues[0];

                if (string.IsNullOrEmpty(appId))
                    appId = Guid.Empty.ToString();

                return appId;
            }
            catch
            {
                return Guid.Empty.ToString();
            }
        }

        public static bool GetAccessPermisionFromUserInfoCookie(string controller, string action)
        {
            try
            {
                HttpCookie userInfoCookie = new HttpCookie(USER_INFO_COOKIE);
                userInfoCookie = HttpContext.Current.Request.Cookies[USER_INFO_COOKIE];
                string[] arrValues = FunctionsManager.Base64DecodeUTF8(userInfoCookie.Value).Split(char.Parse("#"));

                string target = string.Concat(controller, ":", action);
                string accessInfo = arrValues[2];

                string[] arrAccess = accessInfo.ToLower().Split(char.Parse("|"));
                bool has = Array.FindAll(arrAccess, s => s.Equals(target)).Length > 0;
                return has;
            }
            catch
            {
                return false;
            }
        }

        public static Guid UserId
        {
            get
            {
                Guid userId = new Guid();
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(
                               HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                    string[] data = ticket.UserData.Split(new string[] { "||" }, StringSplitOptions.None);
                    Guid.TryParse(data[0], out userId);

                    return userId;
                }
                else
                {
                    throw new AuthenticationException();
                }
            }
        }

        public static string Name
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(
                               HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                    string[] data = ticket.UserData.Split(new string[] { "||" }, StringSplitOptions.None);
                    return data[3];
                }
                else
                {
                    throw new AuthenticationException();
                }
            }
        }

        public static string UserName
        {
            get
            {
                return HttpContext.Current.User.Identity.Name;
            }
        }

        public static string Email
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(
                               HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                    string[] data = ticket.UserData.Split(new string[] { "||" }, StringSplitOptions.None);
                    return data[2];
                }
                else
                {
                    throw new AuthenticationException();
                }
            }
        }

        public static int UserGroupId
        {
            get
            {
                int groupId = 0;
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(
                               HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                    string[] data = ticket.UserData.Split(new string[] { "||" }, StringSplitOptions.None);
                    int.TryParse(data[1], out groupId);

                    return groupId;
                }
                else
                {
                    throw new AuthenticationException();
                }
            }
        }

    }
}