﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReciboNomina.FrontEnd
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "Account",
               "Account/Read/{userName}",
               new { controller = "Account", action = "Read", id = UrlParameter.Optional, userName = "" } 
            );

            routes.MapRoute(
               "Home",
               "Home/Read/{userName}/{company}",
               new { controller = "Home", action = "Read", id = UrlParameter.Optional, userName = "", company = ""}
            );

            routes.MapRoute(
              "GetAllInvoices",
              "Home/GetAllPayrollInvoices/{userName}/{company}/",
              new { controller = "Home", action = "GetAllPayrollInvoices", id = UrlParameter.Optional, userName = "", company = "" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
