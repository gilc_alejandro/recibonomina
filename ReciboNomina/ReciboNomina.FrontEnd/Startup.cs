﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReciboNomina.FrontEnd.Startup))]
namespace ReciboNomina.FrontEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
