﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using ReciboNomina.BusinessManager;
using System.Data;
using System.Configuration;
using ReciboNomina.BODT;

namespace ReciboNomina.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SendInvoice" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SendInvoice.svc or SendInvoice.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SendInvoice : ISendInvoice
    {
        public void DoWork()
        {
        }

        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        public ObjResponseMessage GetInvoicesDidNotSend()
        {
            ObjResponseMessage response = new ObjResponseMessage();
            ResponseMessage responseMessage = new ResponseMessage();

            DataAccessManager dataAccessM = new DataAccessManager();
            Guid id = Guid.Empty;
            id = dataAccessM.GetInvoicesDidNotSend();

            try
            {
                if (id != Guid.Empty)
                {
                    DataTable dt = new DataTable();
                    dt = dataAccessM.GetGeneratedInvoicesByEmployeeById(id);
                    string invoicePath = ConfigurationManager.AppSettings["InvoicePath"];
                    foreach (DataRow row in dt.Rows)
                    {
                        dataAccessM.SendEmail(
                             row.ItemArray[12].ToString().Trim(),
                             row.ItemArray[5].ToString().Trim(),
                             row.ItemArray[11].ToString().Trim(),
                             int.Parse(row.ItemArray[1].ToString().Trim()),
                             DateTime.Parse(row.ItemArray[4].ToString().Trim()).Month,
                             DateTime.Parse(row.ItemArray[4].ToString().Trim()).Day,
                             "",
                             row.ItemArray[6].ToString().Trim()
                        );
                        dataAccessM.SetInvoiceAsSent(id);
                    }
                    response.Success = response.Success;
                    responseMessage.Message = "Los recibos han sido enviados.";
                    response.ResponseMessages.Add(responseMessage);
                }
            }
            catch (Exception ex)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                response.Success = false;
                responseMessage.Message = "Ocurrio un error: " + ex.Message;
                response.ResponseMessages.Add(responseMessage);
            }

            return response;
        }
    }
}
