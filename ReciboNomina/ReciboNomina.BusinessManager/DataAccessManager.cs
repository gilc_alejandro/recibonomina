﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReciboNomina.BODT;
using ReciboNomina.DataManager;
using System.Data;
using Entrepidus.Data;
using System.Net.NetworkInformation;
using System.Net.Mail;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Net;
using System.Resources;
using System.Reflection;

namespace ReciboNomina.BusinessManager
{
    public class DataAccessManager
    {
        public List<ComboItem> GetCompanies(string user)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            List<ComboItem> companiesList = new List<ComboItem>();
            try
            {
                companiesList = dataAccessDM.GetCompanies(user).ToList();
            }
            catch 
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return companiesList;
        }

        public DataTable AuthenticateUser(string userName, string password)
        {
            DataTable dt = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                dt = dataAccessDM.AuthenticateUser(userName, password);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return dt;
        }

        public List<ComboItem> GetPayRollList()
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            List<ComboItem> payRollList = new List<ComboItem>();
            try
            {
                payRollList = dataAccessDM.GetPayRollList().ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return payRollList;
        }

        public List<ComboItem> GetRolesByUser(string userName)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            List<ComboItem> roleList = new List<ComboItem>();
            try
            {
                roleList = dataAccessDM.GetRolesIdForUserName(userName).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return roleList;
        }

        public DataTable GetListValidUserAuthorization(Guid userId, bool validateFunctionality = true)
        {
            DataTable dt = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                dt = dataAccessDM.GetListValidUserAuthorization(userId);

            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return dt;
        }

        public List<ComboItem> GetClassOfPayroll()
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            List<ComboItem> playrollList = new List<ComboItem>();
            try
            {
                playrollList = dataAccessDM.GetClassOfPayroll().ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return playrollList;
        }

        public List<ComboItem> GetTypeOfPayroll()
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            List<ComboItem> playrollList = new List<ComboItem>();
            try
            {
                playrollList = dataAccessDM.GetTypeOfPayroll().ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return playrollList;
        }

        public long CalculateInvoiceByEmployee(IFilter[] filters)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            long ret = 0;
            try
            {
                ret = dataAccessDM.CalculateInvoiceByEmployee(filters);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return ret;
        }

        public DataTable GenerateClassOfPayrollInvoice(IFilter[] filters)
        {
            string session = DateTime.Today.Hour.ToString() + DateTime.Today.Minute.ToString() + DateTime.Today.Second.ToString();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            DataTable dt = new DataTable();
            try
            {
                long result = dataAccessDM.GenerateInvoiceClassOfPayroll(filters);

                //Get Invoices Employees Generated 
               
                dt = dataAccessDM.GetInvoiceEmployeeByCompany(filters.FirstOrDefault(x => x.Name == "i_Compania").Value.ToString());

                foreach (DataRow row in dt.Rows)
                {
                    
                    //GenerateInvoiceByEmployee(generateOn, pathBase, pathReport, filters);
                    //SendEmail(pathBase, filters[12].Value.ToString(), row.ItemArray[1].ToString(), int.Parse(filters[3].Value.ToString()), int.Parse(filters[4].Value.ToString()));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return dt;
        }

        public static DataTable GetPayrollInvoicesByEmployee(IFilter[] filters, Guid? generatedInvoiceId)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            long result = dataAccessDM.CalculateInvoiceByEmployee(filters);
            DataTable getInvoices = new DataTable();
            getInvoices = dataAccessDM.GetPayrollInvoicesByEmployee(filters[0].Value.ToString(), filters[12].Value.ToString(), "T", generatedInvoiceId);
            return getInvoices;
        }

        public static DataTable GetPayrollInvoicesByEmployeeDetails(IFilter[] filters)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            DataTable getInvoicesDetails = new DataTable();
            getInvoicesDetails = dataAccessDM.GetPayrollInvoicesByEmployeeDetails(filters);
            return getInvoicesDetails;
        }

        public void SendEmail(string path, string employee, string email, int year, int month, int day, string typeOfPayroll, string employeeCode)
        {
            ResourceManager man = new ResourceManager("ReciboNomina.BusinessManager.Config", Assembly.GetExecutingAssembly());
            if (man == null)
            {
                throw new Exception("resource not loaded.");
            }
            string senderMail = man.GetString("SenderMail");
            string senderPassword = man.GetString("SenderPassword");
            string server = man.GetString("Server");
            string bbc = man.GetString("BBC");
            int port = int.Parse(man.GetString("Port"));
            try
            {
                using (var client = new SmtpClient(server, port))
                {
                    client.Credentials = new NetworkCredential(senderMail, senderPassword);

                    MailMessage msg = new MailMessage();
                    msg.To.Add(senderMail);
                    msg.From = new MailAddress(senderMail);
                    msg.Bcc.Add(bbc);
                    msg.Subject = String.Format("{0}{1}{2}{3}{4}{5}", year.ToString(), month.ToString(), day.ToString(),"", employeeCode, employee );
                    msg.Body = "Se adjunta Recibo de Pago del mes " + month.ToString();
                    msg.Body = " Por Favor no responder este correo.";
                    msg.BodyEncoding = System.Text.Encoding.UTF8;
                    msg.SubjectEncoding = System.Text.Encoding.UTF8;
                    var attachment = new Attachment(path);
                    msg.Attachments.Add(attachment);
                    client.Send(msg);
                }
            }
            catch
            {
                throw;
            }
        }

        public DataTable SaveEmployeeInvoices(Guid userId, IFilter[] filters)
        {
            DataTable saveEmployeeInvoicesDt = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                saveEmployeeInvoicesDt = dataAccessDM.SaveEmployeeInvoice(userId, filters);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return saveEmployeeInvoicesDt;
        }

        public DataTable GetGeneratedInvoicesByEmployeeById(Guid? id)
        {
            DataTable getGeneratedInvoicesByEmployeeById = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                getGeneratedInvoicesByEmployeeById = dataAccessDM.GetGeneratedInvoicesByEmployeeById(id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return getGeneratedInvoicesByEmployeeById;
        }

        public DataTable GetGeneratedInvoicesByEmployeeById_2(Guid? id)
        {
            DataTable getGeneratedInvoicesByEmployeeById = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                getGeneratedInvoicesByEmployeeById = dataAccessDM.GetGeneratedInvoicesByEmployeeById_2(id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return getGeneratedInvoicesByEmployeeById;
        }

        public static DataTable GetHeaderInvoice(string company, string employee, string type)
        {
            DataTable getHeaderInvoices = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                getHeaderInvoices = dataAccessDM.GetHeaderInvoices(company, employee, type);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return getHeaderInvoices;
        }

        public Guid GetInvoicesDidNotSend()
        {
            Guid id = new Guid();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                id = dataAccessDM.GetInvoicesDidNotSend();
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return id;
        }

        public DataTable GetEmployeesDidNotSendInvoices(Guid id)
        {
            DataTable dt = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                dt = GetEmployeesDidNotSendInvoices(id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return dt;
        }

        public bool SetInvoiceAsSent(Guid id)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            bool ret = false;
            try
            {
                dataAccessDM.SetInvoiceAsSent(id);
            }
            catch 
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return ret;
        }

        public bool InvoiceSave(Guid? id, string path, string employeeFullName)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            bool ret = false;
            try
            {
                dataAccessDM.InvoiceSave(id, path, employeeFullName);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return ret;
        }

        public DataTable GetInvoicesDidNotSendDetails()
        {
            DataTable getInvoicesDidNotSendDetails = new DataTable();
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            try
            {
                getInvoicesDidNotSendDetails = dataAccessDM.GetInvoicesDidNotSendDetails();
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return getInvoicesDidNotSendDetails;
        }

        #region PrivateMethods
        private DataTable GetEmployeeByCompany(string company)
        {
            DataAccessDataManager dataAccessDM = new DataAccessDataManager();
            DataTable ret = new DataTable();
            try
            {
                ret = dataAccessDM.GetInvoiceEmployeeByCompany(company);
            }
            catch
            {
                throw;
            }
            finally
            {
                dataAccessDM.Dispose();
            }
            return ret;
        }

        private bool IsValid(string emailaddress)
        {
            bool ret = false;
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                ret = true;
            }
            catch 
            {
                throw;
            }
            return ret;
        }
        #endregion PrivateMethods
    }
}
