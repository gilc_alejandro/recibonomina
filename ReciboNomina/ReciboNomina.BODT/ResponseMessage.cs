﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ReciboNomina.BODT
{
    [DataContract(Name = "RM")]
    public class ResponseMessage
    {
        [DataMember(Name = "L")]
        public string Lang { get; set; }

        [DataMember(Name = "M")]
        public string Message { get; set; }
    }
}
