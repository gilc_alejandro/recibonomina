﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ReciboNomina.BODT
{
    public class Login
    {
        public Login()
        {

        }

        [Required(ErrorMessageResourceName = "Common_Required", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string User { get; set; }

        [Required(ErrorMessageResourceName = "Common_Required", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }
    }
}
