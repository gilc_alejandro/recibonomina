﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ReciboNomina.BODT
{
    [DataContract(Name = "ORM")]
    public class ObjResponseMessage
    {
        [DataMember(Name = "S")]
        public bool Success { get; set; }

        private List<ResponseMessage> _responseMessages;
        [DataMember(Name = "RMs")]
        public List<ResponseMessage> ResponseMessages
        {
            get
            {
                if (_responseMessages == null)
                    _responseMessages = new List<ResponseMessage>();
                return _responseMessages;
            }
        }
    }
}
