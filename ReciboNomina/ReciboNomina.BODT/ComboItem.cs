﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReciboNomina.BODT
{
    public class ComboItem
    {
        public string Value { get; set; }

        public string Text { get; set; }

        public string Title { get; set; }

        public bool IsSelected { get; set; }

        public ComboItem()
        {
        }

        public ComboItem(string value, string text, string title = "", bool isSelected = false) // This parameter is optional in order to maintain the legacy
        {
            this.Value = value;
            this.Text = text;
            this.Title = title;
            this.IsSelected = isSelected;
        }
    }
}
