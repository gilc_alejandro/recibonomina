﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace ReciboNomina.BODT
{
    public class Recibo
    {
        public Recibo()
        {
            PayrollList = new List<ComboItem>();
            ClassOfPayrollList = new List<ComboItem>();
            TypeOfPayrollList = new List<ComboItem>();
            SelectedTypeOfInvoice = null;
            SelectedGenerateBy = null;
            CompanyList = new List<ComboItem>();
            EmployeeList = new List<ComboItem>();
        }

        public string SESA { get; set; }

        public List<ComboItem> PayrollList { get; set; }

        public List<ComboItem> ClassOfPayrollList { get; set; }

        public List<ComboItem> TypeOfPayrollList { get; set; }

        public List<ComboItem> CompanyList { get; set; }

        public List<ComboItem> EmployeeList { get; set; }

        public string Payroll { get; set; }

        [Required(ErrorMessageResourceName = "Common_Required_ClassOfPayroll", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ClassOfPayroll { get; set; }

        [Required(ErrorMessageResourceName = "Common_Required_TypeOfPayroll", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string TypeOfPayroll { get; set; }

        public List<ComboItem> CompaniesList { get; set; }

        [Required(ErrorMessageResourceName = "Common_Required", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Company { get; set; }

        public string Employee { get; set; }

        [Range(2000, 2100)]
        public int Year { get; set; }

        [Range(1, 24)]
        public int Period { get; set; }

        [Required(ErrorMessageResourceName = "Common_Required_SeletectedTypeOfInvoice", ErrorMessageResourceType = typeof(Resources.Resources))]
        public TypeOfInvoice? SelectedTypeOfInvoice { get; set; }

        public enum TypeOfInvoice
        {
            ByEmail,
            ByScreen
        }

        [Required(ErrorMessageResourceName = "Common_Required_SeletectedGenerateBy", ErrorMessageResourceType = typeof(Resources.Resources))]
        public GenerateBy? SelectedGenerateBy { get; set; }

        public enum GenerateBy
        {
            Employee,
            ClassOfPayroll
        }

        public DataTable InvoicesList { get; set; }

        public string Error { get; set; }
    }
}
